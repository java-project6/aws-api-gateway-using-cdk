FROM alpine:3.19.1
RUN apk add --update --no-cache openjdk17 maven nodejs npm zip
RUN npm i -g aws-cdk
RUN npm i -g aws-cdk-lib
