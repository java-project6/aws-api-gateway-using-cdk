package dev.adafycheng.aws.apigw;

import dev.adafycheng.aws.apigw.config.AppConfig;
import dev.adafycheng.aws.apigw.config.SystemConfig;
import dev.adafycheng.aws.apigw.stack.AwsApiGwCdkStack;
import dev.adafycheng.aws.apigw.stack.SharedInfraCdkStack;
import dev.adafycheng.aws.apigw.stack.SharedStackProps;
import dev.adafycheng.aws.apigw.stack.SqsCdkStack;
import dev.adafycheng.aws.apigw.util.AppConfigLoader;
import java.io.FileNotFoundException;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import software.amazon.awscdk.App;
import software.amazon.awscdk.Environment;
import software.amazon.awscdk.StackProps;


import static dev.adafycheng.aws.apigw.AppConstants.*;


@Slf4j
public class AwsApiGwCdkApp {

  public static void main(final String[] args) {

    final String deploymentEnv = System.getenv("CI_ENVIRONMENT_NAME");
    final String accountId = System.getenv("AWS_ACCOUNT_ID");

    App app = new App();

    try {
      AppConfig appConfig = AppConfigLoader.load(deploymentEnv);

      // 1. Load configurations
      Environment env = Environment.builder()
        .account(accountId)
        .region(appConfig.getRegion())
        .build();

      SystemConfig systemConfig = SystemConfig.builder()
        .vpcId(System.getenv("VPC_ID"))
        .vpcName(System.getenv("VPC_NAME"))
        .build();

      // 2. Create CloudFormation Stacks
      // 2.1 Tags
      Map<String, String> tags = Map.of(
        SERVICE_TAG, SERVICE,
        ENV_TAG, deploymentEnv,
        VERSION_TAG, APP_VERSION
      );

      // 2.2 Shared Infrastructures
      SharedInfraCdkStack sharedInfraCdkStack =new SharedInfraCdkStack(app,
        SHARED_INFRA_STACK_NAME,
        StackProps.builder()
          .env(env)
          .tags(tags)
          .build(),
        systemConfig);

      // 2.3 SQS Infrastructures
      SqsCdkStack sqsCdkStack = new SqsCdkStack(app,
        SQS_STACK_NAME,
        StackProps.builder()
          .env(env)
          .tags(tags)
          .build());

      // 2.4 Shared Stack Properties
      SharedStackProps sharedStackProps = SharedStackProps.builder()
        .vpc(sharedInfraCdkStack.getVpc())
        .queueMap(sqsCdkStack.getQueueMap())
        .build();

      // 2.5 API Gateway
      new AwsApiGwCdkStack(app,
        APIGW_STACK_NAME,
        StackProps.builder()
          .env(env)
          .tags(tags)
          .build(),
        appConfig,
        sharedStackProps);

      // 3. Synth the CDK application
      app.synth();
    } catch (FileNotFoundException e) {
      log.error("Failed to synth application.", e);
    }
  }

}
