package dev.adafycheng.aws.apigw.util;

import dev.adafycheng.aws.apigw.config.SqsQueueConfig;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import software.amazon.awscdk.services.apigateway.*;
import software.amazon.awscdk.services.iam.Role;
import software.amazon.awscdk.services.iam.RoleProps;
import software.amazon.awscdk.services.iam.ServicePrincipal;
import software.amazon.awscdk.services.sqs.Queue;
import software.amazon.awscdk.services.sqs.QueueEncryption;
import software.constructs.Construct;


/**
 * Helper class for creating SQS Integration of AWS API Gateway.
 */
public class SqsIntegrationHelper {

  private static final String SQS_SERVICE = "sqs";
  private static final String SUCCESS_STATUS_CODE = "200";

  /**
   * Create SQS queues.
   * @param scope scope
   * @return map of SQS queue name and its queue instance
   */
  public static Map<String, Queue> createSqsQueues(Construct scope) {
    Map<String, Queue> map = new HashMap<>();

    // Create SQS Queues
    for (SqsQueueConfig queue: SqsQueueConfig.values()) {
      if (queue.isFifo) {
        map.put(queue.queueName,
          Queue.Builder.create(scope, queue.queueName)
            .queueName(queue.queueName)
            .fifo(true)
            .encryption(QueueEncryption.SQS_MANAGED)
            .build());
      } else {
        map.put(queue.queueName,
          Queue.Builder.create(scope, queue.queueName)
            .queueName(queue.queueName)
            .encryption(QueueEncryption.SQS_MANAGED)
            .build());
      }
    }

    return map;
  }

  /**
   * Create Role for SQS Integration.
   * @param scope scope
   * @param roleName role name for the SQS Integration
   * @return IAM Role for the SQS Integration
   */
  public static Role createSqsRole(Construct scope,
                                   String roleName) {
    return new Role(scope, roleName,
      RoleProps.builder()
        .roleName(roleName)
        .assumedBy(new ServicePrincipal("apigateway.amazonaws.com"))
        .inlinePolicies(Map.of())
        .build());
  }

  /**
   * Create a method for the API Gateway Resource.
   */
  public static void addSqsResourceMethod(IResource resource,
                                          String method,
                                          AwsIntegration integration) {

    resource.addMethod(method, integration,
        MethodOptions.builder()
            .apiKeyRequired(true)
            .methodResponses(List.of(MethodResponse.builder()
              .statusCode(SUCCESS_STATUS_CODE)
              .build()))
            .build()
    );
  }

  /**
   * Create Request Integration with the target backend service.
   */
  public static AwsIntegration createSqsIntegration(Queue queue,
                                                    String accountId,
                                                    String method,
                                                    Role role) {

    queue.grantSendMessages(role);

    Map<String, String> requestParameters = Map.of("integration.request.header.Content-Type",
        "'application/x-www-form-urlencoded'");
    Map<String, String> requestTemplates = Map.of("application/json",
        "Action=SendMessage&MessageBody=$input.body");

    return AwsIntegration.Builder.create()
        .service(SQS_SERVICE)
        .path(String.format("%s/%s", accountId, queue.getQueueName()))
        .integrationHttpMethod(method)
          .options(IntegrationOptions.builder()
            .credentialsRole(role)
            .requestParameters(requestParameters)
            .passthroughBehavior(PassthroughBehavior.NEVER)
            .requestTemplates(requestTemplates)
            .integrationResponses(
              List.of(IntegrationResponse.builder()
                .statusCode(SUCCESS_STATUS_CODE)
                .build())
            )
            .build())
        .build();
  }

}
