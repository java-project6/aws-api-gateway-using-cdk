package dev.adafycheng.aws.apigw.config;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


/**
 * Environmental configurations for CloudFormation App.
 */
@Getter
@Setter
@NoArgsConstructor
public class AppConfig {

  // API Gateway Settings
  private String region;
  private String deploymentStage;

}
