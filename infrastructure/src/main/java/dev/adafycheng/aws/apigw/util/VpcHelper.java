package dev.adafycheng.aws.apigw.util;

import dev.adafycheng.aws.apigw.config.SystemConfig;
import software.amazon.awscdk.services.ec2.IVpc;
import software.amazon.awscdk.services.ec2.SecurityGroup;
import software.amazon.awscdk.services.ec2.SecurityGroupProps;
import software.amazon.awscdk.services.ec2.Vpc;
import software.amazon.awscdk.services.ec2.VpcLookupOptions;
import software.constructs.Construct;

public class VpcHelper {

  /**
   * Get existing VPC.
   * @param scope scope
   * @param config configurations
   * @return existing VPC.
   */
  public static IVpc getVpc(Construct scope,
                            SystemConfig config) {
    return Vpc.fromLookup(scope,
      config.getVpcName(),
      VpcLookupOptions.builder()
        .vpcId(config.getVpcId())
        .build());
  }

  /**
   * Create Security Group, disabling all outbounds.
   */
  public static SecurityGroup createSecurityGroup(Construct scope,
                                                  IVpc vpc,
                                                  String sgName,
                                                  String sgDesc) {
    return new SecurityGroup(scope, sgName,
      SecurityGroupProps.builder()
        .description(sgDesc)
        .allowAllOutbound(false)
        .securityGroupName(sgName)
        .vpc(vpc)
        .build()
    );
  }

}
