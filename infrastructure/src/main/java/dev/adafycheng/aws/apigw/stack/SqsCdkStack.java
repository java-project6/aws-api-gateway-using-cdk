package dev.adafycheng.aws.apigw.stack;

import dev.adafycheng.aws.apigw.util.SqsIntegrationHelper;
import java.util.Map;
import lombok.Getter;
import software.amazon.awscdk.Stack;
import software.amazon.awscdk.StackProps;
import software.amazon.awscdk.services.sqs.Queue;
import software.constructs.Construct;


/**
 * Stack for creating SQS Queues.
 */
public class SqsCdkStack extends Stack {

  @Getter
  Map<String, Queue> queueMap;

  public SqsCdkStack(final Construct scope,
                     final String id,
                     final StackProps props) {
    super(scope, id, props);

    // 1. Create SQS queues
    this.queueMap = SqsIntegrationHelper.createSqsQueues(this);

  }

}
