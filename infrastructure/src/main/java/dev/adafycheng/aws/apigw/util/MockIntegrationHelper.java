package dev.adafycheng.aws.apigw.util;

import software.amazon.awscdk.services.apigateway.IntegrationResponse;
import software.amazon.awscdk.services.apigateway.MethodOptions;
import software.amazon.awscdk.services.apigateway.MethodResponse;
import software.amazon.awscdk.services.apigateway.MockIntegration;
import software.amazon.awscdk.services.apigateway.Model;
import software.amazon.awscdk.services.apigateway.PassthroughBehavior;
import software.amazon.awscdk.services.apigateway.Resource;

import java.util.List;
import java.util.Map;

import static dev.adafycheng.aws.apigw.AppConstants.APPLICATION_JSON_TYPE;
import static dev.adafycheng.aws.apigw.AppConstants.HTTP_ANY_METHOD;
import static dev.adafycheng.aws.apigw.AppConstants.NOT_FOUND_STATUS_CODE;

public class MockIntegrationHelper {
  /**
   * Add Mock Integration which returns 404 status code for any other paths not defined in the API Gateway.
   * @param resource resource
   */
  public static void addIntegrationForUndefinedPaths(Resource resource) {
    // Integration Response
    IntegrationResponse.Builder builder = IntegrationResponse.builder().statusCode(NOT_FOUND_STATUS_CODE);

    String lineSeparator = System.lineSeparator();
    builder.responseTemplates(Map.of(
      APPLICATION_JSON_TYPE, "{" + lineSeparator
        + "  \"message\": \"User is not authorized to access this resource with an explicit deny\"" + lineSeparator
        + "}"
    ));

    IntegrationResponse integrationResponse = builder.build();

    // Method Response
    List<MethodResponse> methodResponseList = List.of(MethodResponse.builder()
      .statusCode(NOT_FOUND_STATUS_CODE)
      .responseModels(Map.of(APPLICATION_JSON_TYPE, Model.EMPTY_MODEL))
      .build());

    // Add method to the resource
    resource.addMethod(HTTP_ANY_METHOD,
      MockIntegration.Builder.create()
        .integrationResponses(
          List.of(integrationResponse))
        .passthroughBehavior(PassthroughBehavior.NEVER)
        .requestTemplates(Map.of(
          APPLICATION_JSON_TYPE, String.format("{ \"statusCode\": %s }", NOT_FOUND_STATUS_CODE)))
        .build(),
      MethodOptions.builder()
        .authorizer(null)
        .requestParameters(null)
        .methodResponses(methodResponseList)
        .build()
    );

  }
}
