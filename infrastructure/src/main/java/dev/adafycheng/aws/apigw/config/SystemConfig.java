package dev.adafycheng.aws.apigw.config;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;


/**
 * System configurations for CloudFormation App.
 */
@Getter
@Setter
@Builder
public class SystemConfig {

  // VPC Configurations
  private String vpcId;

  // VPC Configurations
  private String vpcName;

}
