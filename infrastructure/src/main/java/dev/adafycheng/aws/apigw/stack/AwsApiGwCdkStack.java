package dev.adafycheng.aws.apigw.stack;

import dev.adafycheng.aws.apigw.AwsApiGw;
import dev.adafycheng.aws.apigw.config.AppConfig;
import dev.adafycheng.aws.apigw.util.SqsIntegrationHelper;
import software.amazon.awscdk.Stack;
import software.amazon.awscdk.StackProps;
import software.amazon.awscdk.services.apigateway.RequestAuthorizer;
import software.amazon.awscdk.services.apigateway.Resource;
import software.amazon.awscdk.services.iam.Role;
import software.constructs.Construct;

import static dev.adafycheng.aws.apigw.AppConstants.APIGW_SQS_INTEG_ROLE_PATTERN;
import static dev.adafycheng.aws.apigw.AppConstants.API_PATH;
import static dev.adafycheng.aws.apigw.AppConstants.HW_PATH;
import static dev.adafycheng.aws.apigw.AppConstants.SQS_PATH;
import static dev.adafycheng.aws.apigw.AppConstants.VERSION_PATH;


/**
 * Stack for creating AWS API Gateway.
 */
public class AwsApiGwCdkStack extends Stack {

  public AwsApiGwCdkStack(final Construct scope,
                          final String id,
                          final StackProps props,
                          final AppConfig appConfig,
                          final SharedStackProps sharedStackProps) {
    super(scope, id, props);

    // 1. Create AWS API Gateway
    AwsApiGw awsApiGw = new AwsApiGw(this, appConfig, sharedStackProps);
    awsApiGw.createApiGateway();

    // 2. Define API resources
    Resource api = awsApiGw.getRestapi().getRoot().addResource(API_PATH);
    Resource version = api.addResource(VERSION_PATH);

    // 3. Create SQS Integration for AWS API Gateway
    // 3.1 Create SQS Role
    String roleName = String.format(APIGW_SQS_INTEG_ROLE_PATTERN, appConfig.getRegion());
    Role sqsRole = SqsIntegrationHelper.createSqsRole(this,
      roleName);

    // 3.2 Create SQS Integration
    awsApiGw.createSqsIntegration(version,
      SQS_PATH,
      this.getAccount(),
      sqsRole);

    // 4. Hello World Endpoint demonstrating Lambda Authorizer in JavaScript
    // 4.1 Hello World Authorizer
    RequestAuthorizer authorizer = awsApiGw.createHelloWorldAuthorizer();

    // 4.2 Hello World endpoint
    awsApiGw.addHelloWorldEndpoint(version, HW_PATH, authorizer);

    // 5. Mock Integration for all other undefined endpoints should return 404 status code
    awsApiGw.addIntegrationForUndefinedPaths();

  }

}
