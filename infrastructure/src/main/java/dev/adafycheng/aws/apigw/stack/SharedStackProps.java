package dev.adafycheng.aws.apigw.stack;

import java.util.Map;
import lombok.Builder;
import lombok.Getter;
import software.amazon.awscdk.services.ec2.IVpc;
import software.amazon.awscdk.services.sqs.Queue;


@Builder
@Getter
public class SharedStackProps {

  private IVpc vpc;

  private Map<String, Queue> queueMap;

}
