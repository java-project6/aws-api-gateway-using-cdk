/*
 * Copyright (c) Jaguar Land Rover Ltd 2023. All rights reserved
 */

package dev.adafycheng.aws.apigw;

public class AppConstants {

  // Application
  public static final String APP_PREFIX = "new-demo";

  // Tags
  public static final String SERVICE_TAG = "service";
  public static final String SERVICE = String.format("%s-api-gw-service", APP_PREFIX);

  public static final String ENV_TAG = "env";

  public static final String VERSION_TAG = "version";
  public static final String APP_VERSION = "v1.3.0";

  // Stacks
  public static final String SHARED_INFRA_STACK_NAME = String.format("%s-shared-infra", APP_PREFIX);
  public static final String SQS_STACK_NAME = String.format("%s-sqs-stack", APP_PREFIX);
  public static final String APIGW_STACK_NAME = String.format("%s-api-gw-stack", APP_PREFIX);

  // API Gateway
  public static final String APIGW_NAME = String.format("%s-api-gw", APP_PREFIX);
  public static final String APIGW_DESC = "AWS REST API Gateway Demo";
  public static final String APIGW_LOG_GROUP_NAME = String.format("%s-log-group", APIGW_NAME);

  // Constants for API Gateway resources
  public static final String API_PATH = "api";
  public static final String VERSION_PATH = "v1";

  // Constants for API Gateway methods and integration endpoints
  public static final String HTTP_ANY_METHOD = "ANY";
  public static final String HTTP_GET_METHOD = "GET";
  public static final String HTTP_POST_METHOD = "POST";

  // Security Group
  public static final String LAMBDA_SG_NAME = String.format("%s-lambda-sg", APIGW_NAME);
  public static final String LAMBDA_SG_DESC = String.format("Security group for lambda function (%s)", LAMBDA_SG_NAME);

  // DLQ
  public static final String LAMBDA_DLQ_NAME = String.format("%s-lambda-dlq", APIGW_NAME);

  // Usage Plans
  public static final String APIGW_USAGE_PLAN_NAME = String.format("%s-usage-plan", APIGW_NAME);
  public static final String APIGW_API_KEY_NAME = String.format("%s-api-key", APIGW_NAME);
  public static final String APIGW_API_KEY_DESC = String.format("API Key for %s", APIGW_NAME);
  public static final int APIGW_USAGE_PLAN_RATE_LIMIT = 10;
  public static final int APIGW_USAGE_PLAN_BURST_LIMIT = 2;

  public static final String ROLE_NAME_SUFFIX = "-role";

  // Content Types
  public static final String APPLICATION_JSON_TYPE = "application/json";

  // Status Codes
  public static final String SUCCESS_STATUS_CODE = "200";
  public static final String NOT_FOUND_STATUS_CODE = "404";

  public static final String API_PROXY_PATH = "{proxy+}";

  // SQS Integration
  public static final String SQS_PATH = "sqs";
  public static final String APIGW_SQS_INTEG_NAME = String.format("%s-sqs-integ", APIGW_NAME);
  public static final String APIGW_SQS_INTEG_ROLE_PATTERN = APIGW_SQS_INTEG_NAME
      + "-%s" + ROLE_NAME_SUFFIX;

  // Hello World endpoint
  public static final String HW_PATH = "hello";
  public static final String HW_AUTHORIZER_NAME = "hello-world-authorizer";
  public static final String HW_AUTHORIZER_FUNCTION_NAME = String.format("%s-function",
      HW_AUTHORIZER_NAME);


}
