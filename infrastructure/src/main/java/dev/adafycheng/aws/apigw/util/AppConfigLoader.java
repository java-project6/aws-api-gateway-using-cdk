/*
 * Copyright (c) Jaguar Land Rover Ltd 2023. All rights reserved
 */

package dev.adafycheng.aws.apigw.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.UnrecognizedPropertyException;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import dev.adafycheng.aws.apigw.config.AppConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;


public class AppConfigLoader {

  private static final Logger LOGGER = LoggerFactory.getLogger(AppConfigLoader.class);

  /**
   * Load configurations from YAML file.
   * @param configEnv environment name for the configurations
   *                  This function loads the configuration file specified by the configEnv
   *                  with ".yaml" extension from the "resources/config" directory.
   * @return configurations
   */
  public static AppConfig load(String configEnv) throws FileNotFoundException {

    if (configEnv == null || configEnv.isEmpty()) {
      throw new IllegalArgumentException("No environment is provided.");
    }

    try {
      File file = new File(AppConfig.class.getClassLoader().getResource(String.format("config/%s.yaml", configEnv)).getFile());

      // Instantiating a new ObjectMapper as a YAMLFactory
      ObjectMapper om = new ObjectMapper(new YAMLFactory());

      return om.readValue(file, AppConfig.class);
    } catch (UnrecognizedPropertyException e) {
      LOGGER.info("Error when parsing the configuration file", e);
      throw new FileNotFoundException("Error when parsing the configuration file.");
    } catch (IOException e) {
      throw new FileNotFoundException("No configuration file found.");
    }
  }

}
