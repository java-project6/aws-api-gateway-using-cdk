package dev.adafycheng.aws.apigw.stack;

import dev.adafycheng.aws.apigw.config.SystemConfig;
import dev.adafycheng.aws.apigw.util.VpcHelper;
import lombok.Getter;
import software.amazon.awscdk.Stack;
import software.amazon.awscdk.StackProps;
import software.amazon.awscdk.services.ec2.IVpc;
import software.constructs.Construct;


/**
 * Stack for creating global resources.
 */
public class SharedInfraCdkStack extends Stack {

  @Getter
  private IVpc vpc;

  public SharedInfraCdkStack(final Construct scope,
                             final String id,
                             final StackProps props,
                             final SystemConfig systemConfig) {
    super(scope, id, props);

    // 1. Get VPC
    this.vpc = VpcHelper.getVpc(this, systemConfig);

  }

}
