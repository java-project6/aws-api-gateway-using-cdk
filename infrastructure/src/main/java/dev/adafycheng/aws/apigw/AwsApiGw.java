package dev.adafycheng.aws.apigw;

import dev.adafycheng.aws.apigw.config.AppConfig;
import dev.adafycheng.aws.apigw.config.SqsQueueConfig;
import dev.adafycheng.aws.apigw.stack.SharedStackProps;
import dev.adafycheng.aws.apigw.util.AwsApiGwHelper;
import dev.adafycheng.aws.apigw.util.MockIntegrationHelper;
import dev.adafycheng.aws.apigw.util.SqsIntegrationHelper;
import lombok.Getter;
import software.amazon.awscdk.services.apigateway.AwsIntegration;
import software.amazon.awscdk.services.apigateway.IdentitySource;
import software.amazon.awscdk.services.apigateway.IntegrationResponse;
import software.amazon.awscdk.services.apigateway.MethodOptions;
import software.amazon.awscdk.services.apigateway.MethodResponse;
import software.amazon.awscdk.services.apigateway.MockIntegration;
import software.amazon.awscdk.services.apigateway.RequestAuthorizer;
import software.amazon.awscdk.services.apigateway.Resource;
import software.amazon.awscdk.services.apigateway.RestApi;
import software.amazon.awscdk.services.iam.Role;
import software.amazon.awscdk.services.lambda.AssetCode;
import software.amazon.awscdk.services.lambda.Function;
import software.amazon.awscdk.services.lambda.Runtime;
import software.constructs.Construct;

import java.util.List;
import java.util.Map;

import static dev.adafycheng.aws.apigw.AppConstants.APIGW_API_KEY_DESC;
import static dev.adafycheng.aws.apigw.AppConstants.APIGW_API_KEY_NAME;
import static dev.adafycheng.aws.apigw.AppConstants.APIGW_USAGE_PLAN_BURST_LIMIT;
import static dev.adafycheng.aws.apigw.AppConstants.APIGW_USAGE_PLAN_NAME;
import static dev.adafycheng.aws.apigw.AppConstants.APIGW_USAGE_PLAN_RATE_LIMIT;
import static dev.adafycheng.aws.apigw.AppConstants.API_PROXY_PATH;
import static dev.adafycheng.aws.apigw.AppConstants.APPLICATION_JSON_TYPE;
import static dev.adafycheng.aws.apigw.AppConstants.HTTP_GET_METHOD;
import static dev.adafycheng.aws.apigw.AppConstants.HTTP_POST_METHOD;
import static dev.adafycheng.aws.apigw.AppConstants.HW_AUTHORIZER_FUNCTION_NAME;
import static dev.adafycheng.aws.apigw.AppConstants.HW_AUTHORIZER_NAME;
import static dev.adafycheng.aws.apigw.AppConstants.SUCCESS_STATUS_CODE;


public class AwsApiGw {

  private final Construct scope;

  private final AppConfig appConfig;

  private final SharedStackProps sharedStackProps;

  @Getter
  private RestApi restapi;


  public AwsApiGw(Construct scope,
                  AppConfig appConfig,
                  SharedStackProps sharedStackProps) {
    this.scope = scope;
    this.appConfig = appConfig;
    this.sharedStackProps = sharedStackProps;
  }

  /**
   * Create AWS REST API Gateway.
   */
  public void createApiGateway() {
    // 1. Create APi Gateway
    this.restapi = AwsApiGwHelper.createRestApi(scope,
      appConfig,
      AppConstants.APIGW_NAME,
      AppConstants.APIGW_DESC,
      AppConstants.APIGW_LOG_GROUP_NAME);

    // 2. Usage Plan
    AwsApiGwHelper.createUsagePlan(restapi,
      APIGW_USAGE_PLAN_NAME,
      APIGW_USAGE_PLAN_RATE_LIMIT,
      APIGW_USAGE_PLAN_BURST_LIMIT,
      APIGW_API_KEY_NAME,
      APIGW_API_KEY_DESC);
  }

  /**
   * Create SQS Integration.
   * @param versionResource resource for version endpoint
   * @param path path name for the SQS integration
   * @param accountId AWS account ID
   * @param sqsRole role for SQS integration of the API Gateway
   */
  public void createSqsIntegration(Resource versionResource,
                                   String path,
                                   String accountId,
                                   Role sqsRole) {
    // SQS endpoint
    Resource sqs = versionResource.addResource(path);

    // SQS POST
    AwsIntegration integration = SqsIntegrationHelper.createSqsIntegration(
      sharedStackProps.getQueueMap().get(SqsQueueConfig.API_REQUEST.queueName),
      accountId,
      HTTP_POST_METHOD,
      sqsRole);
    SqsIntegrationHelper.addSqsResourceMethod(sqs,
      HTTP_POST_METHOD,
      integration);
  }

  /**
   * Create Hello World Lambda Authorizer in JavaScript.
   * @return Hello World Lambda Authorizer
   */
  public RequestAuthorizer createHelloWorldAuthorizer() {
    Function authorizerFn = Function.Builder.create(scope, HW_AUTHORIZER_FUNCTION_NAME)
      .functionName(HW_AUTHORIZER_FUNCTION_NAME)
      .runtime(Runtime.NODEJS_18_X)
      .handler("index.handler")
      .code(AssetCode.fromAsset("../software/lambda-authorizer-nodejs/build/lambda.zip"))
      .build();

    return RequestAuthorizer.Builder.create(scope, HW_AUTHORIZER_NAME)
      .authorizerName(HW_AUTHORIZER_NAME)
      .handler(authorizerFn)
      .identitySources(List.of(IdentitySource.header("Authorization")))
      .build();
  }

  /**
   * Add Hello World endpoint to the API Gateway
   * @param versionResource the resource for version
   * @param path endpoint path
   * @param authorizer request authorizer for this endpoint
   */
  public void addHelloWorldEndpoint(Resource versionResource,
                                    String path,
                                    RequestAuthorizer authorizer) {
    // Hello World endpoint
    Resource hello = versionResource.addResource(path);

    // Integration Response
    IntegrationResponse.Builder builder = IntegrationResponse.builder().statusCode(SUCCESS_STATUS_CODE);
    String lineSeparator = System.lineSeparator();
    builder.responseTemplates(Map.of(
      APPLICATION_JSON_TYPE, "{" + lineSeparator
        + "  \"message\": \"Hello World!!!\"" + lineSeparator
        + "}"
    ));
    IntegrationResponse integrationResponse = builder.build();

    hello.addMethod(HTTP_GET_METHOD,
      MockIntegration.Builder.create()
        .integrationResponses(List.of(integrationResponse))
        .requestTemplates(Map.of(
          "application/json", String.format("{ \"statusCode\": %s }", SUCCESS_STATUS_CODE)))
        .build(),
      MethodOptions.builder()
        .methodResponses(List.of(MethodResponse.builder().statusCode(SUCCESS_STATUS_CODE).build()))
        .authorizer(authorizer)
        .build());
  }

  /**
   * Add Mock Integration which returns 404 status code for any other paths not defined in the API Gateway.
   */
  public void addIntegrationForUndefinedPaths() {
    Resource proxyResource = restapi.getRoot().addResource(API_PROXY_PATH);
    MockIntegrationHelper.addIntegrationForUndefinedPaths(proxyResource);
  }
}
