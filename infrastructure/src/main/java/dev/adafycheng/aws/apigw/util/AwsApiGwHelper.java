package dev.adafycheng.aws.apigw.util;

import dev.adafycheng.aws.apigw.config.AppConfig;
import java.util.List;
import software.amazon.awscdk.services.apigateway.*;
import software.amazon.awscdk.services.logs.LogGroup;
import software.amazon.awscdk.services.logs.LogGroupProps;
import software.amazon.awscdk.services.logs.RetentionDays;
import software.constructs.Construct;


/**
 * Helper class for creating SQS Integration of AWS API Gateway.
 */
public class AwsApiGwHelper {

  /**
   * Create AWS API Gateway.
   */
  public static RestApi createRestApi(Construct scope,
                                      AppConfig config,
                                      String apiGwName,
                                      String apiGwDesc,
                                      String logGroupName) {

    // Enable CloudWatch for API Gateway
    LogGroup logGroup = AwsApiGwHelper.createLogGroup(scope, logGroupName);

    StageOptions stageOptions = StageOptions.builder()
      .stageName(config.getDeploymentStage())
      .cachingEnabled(false)
      .cacheClusterEnabled(false)
      .accessLogDestination(new LogGroupLogDestination(logGroup))
      .metricsEnabled(true)
      .tracingEnabled(true)
      .dataTraceEnabled(true)
      .build();

    // API Gateway
    return new RestApi(scope,
      apiGwName,
      RestApiProps.builder()
        .restApiName(apiGwName)
        .cloudWatchRole(true)
        .deployOptions(stageOptions)
        .description(apiGwDesc)
        .endpointTypes(List.of(EndpointType.REGIONAL))
        .build());
  }

  /**
   * Create log group for CloudWatch.
   */
  public static LogGroup createLogGroup(Construct scope,
                                        String logGroupName) {
    return new LogGroup(scope, logGroupName,
      LogGroupProps.builder()
        .logGroupName(logGroupName)
        .retention(RetentionDays.ONE_WEEK)
        .build());
  }

  public static void createUsagePlan(RestApi restapi,
                                     String usagePlanName,
                                     int usagePlanRateLimit,
                                     int usagePlanBurstLimit,
                                     String apiKeyName,
                                     String apiKeyDesc) {
    // Usage Plan
    UsagePlan plan = restapi.addUsagePlan(usagePlanName, UsagePlanProps.builder()
      .name(usagePlanName)
      .throttle(ThrottleSettings.builder()
        .rateLimit(usagePlanRateLimit)
        .burstLimit(usagePlanBurstLimit)
        .build())
      .build());
    IApiKey apiKey = restapi.addApiKey(apiKeyName, ApiKeyOptions.builder()
        .apiKeyName(apiKeyName)
        .description(apiKeyDesc)
      .build());
    plan.addApiKey(apiKey);
    plan.addApiStage(UsagePlanPerApiStage.builder()
      .stage(restapi.getDeploymentStage())
      .build());
  }


}
