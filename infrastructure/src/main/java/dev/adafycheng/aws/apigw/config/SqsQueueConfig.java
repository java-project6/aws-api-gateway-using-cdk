package dev.adafycheng.aws.apigw.config;

import java.util.HashMap;
import java.util.Map;

public enum SqsQueueConfig {
  API_REQUEST("demo-api-request", false),            // API Gateway Request
  API_RESPONSE("demo-api-response", false),          // API Gateway Response
  API_REQUEST_FIFO("demo-api-request.fifo", true),   // FIFO API Gateway Request
  API_RESPONSE_FIFO("demo-api-response.fifo", true); // FIFO API Gateway Response

  private static final Map<String, SqsQueueConfig> BY_NAME = new HashMap<>();
  private static final Map<Boolean, SqsQueueConfig> BY_FIFO = new HashMap<>();

  static {
    for (SqsQueueConfig e : values()) {
      BY_NAME.put(e.queueName, e);
      BY_FIFO.put(e.isFifo, e);
    }
  }

  public final String queueName;
  public final boolean isFifo;

  SqsQueueConfig(String name, boolean fifo) {
    this.queueName = name;
    this.isFifo = fifo;
  }

}
