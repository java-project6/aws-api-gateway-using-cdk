package dev.adafycheng.aws.apigw.stack;

import java.io.FileNotFoundException;
import java.util.Map;

import dev.adafycheng.aws.apigw.config.AppConfig;
import dev.adafycheng.aws.apigw.config.SystemConfig;
import dev.adafycheng.aws.apigw.util.AppConfigLoader;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import software.amazon.awscdk.App;
import software.amazon.awscdk.Environment;
import software.amazon.awscdk.StackProps;
import software.amazon.awscdk.assertions.Template;

import static dev.adafycheng.aws.apigw.AppConstants.SHARED_INFRA_STACK_NAME;
import static dev.adafycheng.aws.apigw.AppConstants.SQS_STACK_NAME;


class AwsApiGwCdkStackTest {

  @Test
  void testApiGatewayStack() throws FileNotFoundException, NullPointerException {
    App app = new App();

    String accountId = "some-account-id";
    String deploymentEnv = "test";

    AppConfig appConfig = AppConfigLoader.load(deploymentEnv);

    // 1. Load configurations
    Environment env = Environment.builder()
      .account(accountId)
      .region(appConfig.getRegion())
      .build();

    SystemConfig systemConfig = SystemConfig.builder()
      .vpcId("test-vpc-id")
      .vpcName("test-vpc-name")
      .build();

    SharedInfraCdkStack sharedInfraCdkStack =new SharedInfraCdkStack(app,
      SHARED_INFRA_STACK_NAME,
      StackProps.builder()
        .env(env)
        .build(),
      systemConfig);

    SqsCdkStack sqsCdkStack = new SqsCdkStack(app,
      SQS_STACK_NAME,
      StackProps.builder()
        .env(env)
        .build());

    SharedStackProps sharedStackProps = SharedStackProps.builder()
      .vpc(sharedInfraCdkStack.getVpc())
      .encryptKeySqs(sqsCdkStack.getEncryptKeySqs())
      .queueMap(sqsCdkStack.getQueueMap())
      .build();

    AwsApiGwCdkStack stack = new AwsApiGwCdkStack(app,
      "test",
      StackProps.builder().build(),
      appConfig,
      sharedStackProps);

    Template template = Template.fromStack(stack);

    // API Gateway deployment stage
    template.hasResourceProperties("AWS::ApiGateway::Stage", Map.of(
      "TracingEnabled", true
    ));

    Assertions.assertNotNull(template);
  }

}
