package dev.adafycheng.aws.apigw;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;


class AwsApiGwCdkAppTest {

  @Test
  void testApp() {
    assertThrows(IllegalArgumentException.class, () -> AwsApiGwCdkApp.main(null));
  }

}
