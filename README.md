# AWS API Gateway using CDK

## Overview

This is an AWS CDK v2 project using Java which creates an AWS API Gateway for demonstration purpose.

### Functions

This API Gateway provides the following functions:
1. A POST endpoint for sending request to SQS queues.  The endpoint is protected by API Key defined in Usage Plan.
2. A GET endpoint for hello world request.  The endpoint is protected by a lambda authorizer written in Node.js.

### Deployment

As a personal project with limited resources, since only one AWS account is available, the API Gateway is deployed to different stages in different environment as follows:

| Deployment Environment | Stage in API Gateway | Region in API Gateway |
|:-----------------------|:--------------------:|:---------------------:|
| development            |         dev          |       eu-west-1       |
| staging                |       staging        |       eu-west-3       |
| production             |         prod         |       eu-west-2       |

### Cost

To minimize the cost for this demo API Gateway,
1. Amazon SQS Key (SSE-SQS) is used for the Server-Side Encryption of the SQS queues.
2. Caching is disabled in the API Gateway.
3. No encryption is used in the CloudWatch Log Groups since customer KMS introduces cost on hourly basis.


## Development

### CDK CLI

* Install AWS CDK CLI.

```shell
npm install -g aws-cdk@latest
```

* Initiate CDK project in Java language.

```shell
mkdir infrastructure
cd infrastructure
cdk init app --language java
```
