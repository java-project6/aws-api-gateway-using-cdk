exports.handler = (event, context, callback) => {
    const principalId = 'aflaf78fd7afalnv';

    console.log('Received event:', JSON.stringify(event, null, 2));
    var headers = event.headers;
    var token = (headers != undefined && headers.Authorization != undefined)?headers.Authorization:null;
    
    console.log('token: ' + token);
    // Use token
    if (token == 'Bearer allow') {
        console.log('1. allow');
        const policy = genPolicy('allow', event.methodArn);

        const context = {
            //simpleAuth: true
        };
        const response = {
            principalId: principalId,
            policyDocument: policy,
            context: context
        };
        console.log(event.methodArn);
        callback(null, response);
    } else if (token == 'Bearer deny') {
        console.log('2. deny');
        const policy = genPolicy('deny', event.methodArn);
        const context = {
            //simpleAuth: true
        };
        const response = {
            principalId: principalId,
            policyDocument: policy,
            context: context
        };
        console.log(event.methodArn);
        callback(null, response);
    } else {
        console.log('3. Unauthorized');
        callback('Unauthorized');
    }
};

function genPolicy(effect, resource) {
    const policy = {};
    policy.Version = '2012-10-17';
    policy.Statement = [];
    const stmt = {};
    stmt.Action = 'execute-api:Invoke';
    stmt.Effect = effect;
    stmt.Resource = resource;
    policy.Statement.push(stmt);
    return policy;
}