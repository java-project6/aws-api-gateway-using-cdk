const authorizer = require('./index');

describe('should verify the token', () => {
    
    it('allow token', async () => {
        const token = 'allow';
        const event = {
            headers: {
                Authorization: `Bearer ${token}`
            }
        };

        await authorizer.handler(event, 'context', (error, data) => {
            expect(data.policyDocument.Statement[0].Effect).toBe('allow');
        });
    });

    it('deny token', async () => {
        const token = 'deny';
        const event = {
            headers: {
                Authorization: `Bearer ${token}`
            }
        };

        await authorizer.handler(event, 'context', (error, data) => {
            expect(data.policyDocument.Statement[0].Effect).toBe('deny');
        });
    });

    it('unauthorized', async () => {
        const token = 'any-other-token';
        const event = {
            headers: {
                Authorization: `Bearer ${token}`,
            }
        };

        await authorizer.handler(event, 'context', (error, data) => {
            expect(error).toBe('Unauthorized');
        });
    });

    it('unauthorized without headers', async () => {
        const token = 'any-other-token';
        const event = {};

        await authorizer.handler(event, 'context', (error, data) => {
            expect(error).toBe('Unauthorized');
        });
    });
});

/*
test('allow token', () => {
    event.authorizationToken = 'allow'
    expect(handler(event, context)).toBe(3);
});*/
